<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

class CommentTest extends TestCase
{

    public function testCommentsGetResponseOk()
    {
        $this->withoutMiddleware();
        $response = $this->get('/tasks/1/comments');
        $response->assertStatus(Response::HTTP_OK);
    }

    public function testCommentsPostResponseCreated()
    {
        $this->withoutMiddleware();
        $response = $this->json(
            'POST',
            '/tasks/1/comments',
            [
                'user_id' => 1,
                'task_id' => 1,
                'description' => 'Test comment',
            ]
        );

        $response->assertStatus(Response::HTTP_CREATED);
    }

}
