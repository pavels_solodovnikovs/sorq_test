<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

class TaskTest extends TestCase
{

    public function testTasksGetResponseOk()
    {
        $this->withoutMiddleware();
        $response = $this->get('/tasks');
        $response->assertStatus(Response::HTTP_OK);
    }

    public function testTasksPostResponseCreated()
    {
        $this->withoutMiddleware();
        $response = $this->json(
            'POST',
            '/tasks',
            [
                'title' => 'Test Task',
                'description' => 'Test Description',
                'reporter_id' => 1,
                'assignee_id' => 2,
            ]
        );

        $response->assertStatus(Response::HTTP_CREATED);
    }

}
