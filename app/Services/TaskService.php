<?php

namespace TaskSystem\Services;

use TaskSystem\Factories\TaskFactory;
use TaskSystem\Mappers\CommentMapper;
use TaskSystem\Mappers\TaskMapper;
use TaskSystem\Repositories\TaskRepository;

/**
 * Class TaskService
 * @package TaskSystem\Services
 */
class TaskService
{

    /**
     * @var TaskRepository
     */
    private $taskRepository;

    /**
     * @var TaskFactory
     */
    private $taskFactory;

    /**
     * @var TaskMapper
     */
    private $taskMapper;

    /**
     * @var CommentMapper
     */
    private $commentMapper;

    /**
     * TaskService constructor.
     * @param TaskRepository $taskRepository
     * @param TaskFactory $taskFactory
     * @param TaskMapper $taskMapper
     * @param CommentMapper $commentMapper
     */
    public function __construct(
        TaskRepository $taskRepository,
        TaskFactory $taskFactory,
        TaskMapper $taskMapper,
        CommentMapper $commentMapper
    ) {
        $this->taskRepository = $taskRepository;
        $this->taskFactory = $taskFactory;
        $this->taskMapper = $taskMapper;
        $this->commentMapper = $commentMapper;
    }

    /**
     * @param array $filters
     * @return array|\Illuminate\Database\Eloquent\Collection|mixed|TaskRepository[]
     */
    public function getTasks(array $filters = null)
    {
        $tasks = $this->taskRepository->get($filters);
        if (count($tasks)) {
            return $this->taskMapper->multipleToArray($tasks);
        }
        return $tasks;
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function getTaskById($id)
    {
        $task = $this->taskRepository->getById($id);
        if ($task) {
            return $this->taskMapper->toArray($task);
        }
        return $task;
    }

    /**
     * @param $taskId
     * @return array
     */
    public function getTaskCommentsByTaskId($taskId)
    {
        $task = $this->taskRepository->getById($taskId);
        if ($task) {
            return $this->commentMapper->multipleToArray($task->comments);
        }

        return [];
    }

    /**
     * @param array $data
     * @return bool
     */
    public function addTask(array $data)
    {
        $task = $this->taskFactory->createFromArray($data);
        return $this->taskRepository->add($task);
    }

}
