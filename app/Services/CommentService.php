<?php

namespace TaskSystem\Services;

use TaskSystem\Factories\CommentFactory;
use TaskSystem\Mappers\CommentMapper;
use TaskSystem\Repositories\CommentRepository;

/**
 * Class CommentService
 * @package TaskSystem\Services
 */
class CommentService
{

    /**
     * @var CommentRepository
     */
    private $commentRepository;

    /**
     * @var CommentFactory
     */
    private $commentFactory;

    /**
     * @var CommentMapper
     */
    private $commentMapper;

    /**
     * CommentService constructor.
     * @param CommentRepository $commentRepository
     * @param CommentFactory $commentFactory
     * @param CommentMapper $commentMapper
     */
    public function __construct(
        CommentRepository $commentRepository,
        CommentFactory $commentFactory,
        CommentMapper $commentMapper
    ) {
        $this->commentRepository = $commentRepository;
        $this->commentFactory = $commentFactory;
        $this->commentMapper = $commentMapper;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function addComment(array $data)
    {
        $comment = $this->commentFactory->createFromArray($data);
        return $this->commentRepository->add($comment);
    }

}
