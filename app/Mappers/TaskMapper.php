<?php

namespace TaskSystem\Mappers;

use Illuminate\Database\Eloquent\Collection;
use TaskSystem\Models\Task;

/**
 * Class TaskMapper
 * @package TaskSystem\Mappers
 */
class TaskMapper
{

    const FIELD_ID = 'id';
    const FIELD_TITLE = 'title';
    const FIELD_DESCRIPTION = 'description';
    const FIELD_REPORTER_ID = 'reporter_id';
    const FIELD_ASSIGNEE_ID = 'assignee_id';

    /**
     * @param Task $task
     * @return array
     */
    public function toArray(Task $task)
    {
        $result = [];
        $result[self::FIELD_ID] = $task->id ?? '';
        $result[self::FIELD_TITLE] = $task->title ?? '';
        $result[self::FIELD_DESCRIPTION] = $task->description ?? '';
        $result[self::FIELD_REPORTER_ID] = $task->reporter_id ?? '';
        $result[self::FIELD_ASSIGNEE_ID] = $task->assignee_id ?? '';

        return $result;
    }

    /**
     * @param Collection $collection
     * @return array
     */
    public function multipleToArray(Collection $collection): array
    {
        $result = [];
        foreach ($collection as $item) {
            $result[] = $this->toArray($item);
        }

        return $result;
    }

}
