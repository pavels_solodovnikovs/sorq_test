<?php

namespace TaskSystem\Mappers;

use Illuminate\Database\Eloquent\Collection;
use TaskSystem\Models\Comment;

/**
 * Class CommentMapper
 * @package TaskSystem\Mappers
 */
class CommentMapper
{
    const FIELD_ID = 'id';
    const FIELD_TASK_ID = 'task_id';
    const FIELD_USER_ID = 'user_id';
    const FIELD_DESCRIPTION = 'description';

    /**
     * @param Comment $comment
     * @return array
     */
    public function toArray(Comment $comment)
    {
        $result = [];
        $result[self::FIELD_ID] = $comment->id ?? '';
        $result[self::FIELD_TASK_ID] = $comment->task_id ?? '';
        $result[self::FIELD_USER_ID] = $comment->user_id ?? '';
        $result[self::FIELD_DESCRIPTION] = $comment->description ?? '';

        return $result;
    }

    /**
     * @param Collection $collection
     * @return array
     */
    public function multipleToArray(Collection $collection): array
    {
        $result = [];
        foreach ($collection as $item) {
            $result[] = $this->toArray($item);
        }

        return $result;
    }

}
