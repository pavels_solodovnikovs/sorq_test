<?php

namespace TaskSystem\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use TaskSystem\Services\CommentService;
use TaskSystem\Services\TaskService;

/**
 * Class TaskController
 * @package TaskSystem\Http\Controllers
 */
class TaskController extends BaseController
{
    /**
     * @var TaskService
     */
    private $taskService;
    /**
     * @var CommentService
     */
    private $commentService;

    /**
     * TaskController constructor.
     * @param TaskService $taskService
     * @param CommentService $commentService
     */
    public function __construct(
        TaskService $taskService,
        CommentService $commentService
    ) {
        $this->taskService = $taskService;
        $this->commentService = $commentService;
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $filters = $request->all();
        $tasks = $this->taskService->getTasks($filters);
        return $this->respondOk($tasks);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required|string|max:255',
            'reporter_id' => 'required|integer',
            'assignee_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->respondBadRequest($validator->messages());
        }

        $response = $this->taskService->addTask($data);

        return $response ? $this->respondCreated($response) : $this->respondBadRequest($request);
    }

}
