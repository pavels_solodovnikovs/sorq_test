<?php

namespace TaskSystem\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use TaskSystem\Services\CommentService;
use TaskSystem\Services\TaskService;

/**
 * Class TaskController
 * @package TaskSystem\Http\Controllers
 */
class CommentController extends BaseController
{
    /**
     * @var TaskService
     */
    private $taskService;
    /**
     * @var CommentService
     */
    private $commentService;

    /**
     * TaskController constructor.
     * @param TaskService $taskService
     * @param CommentService $commentService
     */
    public function __construct(
        TaskService $taskService,
        CommentService $commentService
    ) {
        $this->taskService = $taskService;
        $this->commentService = $commentService;
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTaskComments(Request $request)
    {
        $data = [];
        $data['task_id'] = $request->route('task_id');

        $validator = Validator::make($data, [
            'task_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->respondBadRequest($validator->messages());
        }

        $comments = $this->taskService->getTaskCommentsByTaskId($data['task_id']);
        return $this->respondOk($comments);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addTaskComment(Request $request)
    {
        $taskId = $request->route('task_id');
        $data = $request->all();
        $data['task_id'] = $taskId;

        $validator = Validator::make($data, [
            'task_id' => 'required|integer',
            'user_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->respondBadRequest($validator->messages());
        }

        $response = $this->commentService->addComment($data);
        return $response ? $this->respondCreated($response) : $this->respondBadRequest($response);
    }

}
