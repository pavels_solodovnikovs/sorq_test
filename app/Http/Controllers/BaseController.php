<?php

namespace TaskSystem\Http\Controllers;

use Illuminate\Http\Response;

/**
 * Class BaseController
 * @package TaskSystem\Http\Controllers
 */
class BaseController extends Controller
{

    /**
     * HTTP_OK 200
     *
     * @param $response
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondOk($response)
    {
        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * HTTP_CREATED 201
     *
     * @param $response
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondCreated($response)
    {
        return response()->json($response, Response::HTTP_CREATED);
    }

    /**
     * HTTP_BAD_REQUEST 400
     *
     * @param $response
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondBadRequest($response)
    {
        return response()->json($response, Response::HTTP_BAD_REQUEST);
    }

}
