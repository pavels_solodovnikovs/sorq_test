<?php

namespace TaskSystem\Factories;

use TaskSystem\Mappers\CommentMapper;
use TaskSystem\Models\Comment;

/**
 * Class CommentFactory
 * @package TaskSystem\Factories
 */
class CommentFactory
{
    /**
     * @var CommentMapper
     */
    private $mapper;

    /**
     * CommentFactory constructor.
     * @param CommentMapper $mapper
     */
    public function __construct(CommentMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @return Comment
     */
    function create(): Comment
    {
        return new Comment();
    }

    /**
     * @param array $data
     * @return Comment
     */
    function createFromArray(array $data): Comment
    {
        return $this->hydrateFromArray($this->create(), $data);
    }

    /**
     * @param $comment
     * @param array $data
     * @return Comment
     */
    function hydrateFromArray($comment, array $data): Comment
    {
        if (isset($data[CommentMapper::FIELD_TASK_ID])) {
            $comment->task_id = $data[CommentMapper::FIELD_TASK_ID];
        }
        if (isset($data[CommentMapper::FIELD_USER_ID])) {
            $comment->user_id = $data[CommentMapper::FIELD_USER_ID];
        }
        if (isset($data[CommentMapper::FIELD_DESCRIPTION])) {
            $comment->description = $data[CommentMapper::FIELD_DESCRIPTION];
        }

        return $comment;
    }

}
