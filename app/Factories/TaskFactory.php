<?php

namespace TaskSystem\Factories;

use TaskSystem\Mappers\TaskMapper;
use TaskSystem\Models\Task;

/**
 * Class TaskFactory
 * @package TaskSystem\Factories
 */
class TaskFactory
{
    /**
     * @var TaskMapper
     */
    private $mapper;

    /**
     * TaskFactory constructor.
     * @param TaskMapper $mapper
     */
    public function __construct(TaskMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @return Task
     */
    function create(): Task
    {
        return new Task();
    }

    /**
     * @param array $data
     * @return Task
     */
    function createFromArray(array $data): Task
    {
        return $this->hydrateFromArray($this->create(), $data);
    }

    /**
     * @param $task
     * @param array $data
     * @return Task
     */
    function hydrateFromArray($task, array $data): Task
    {
        if (isset($data[TaskMapper::FIELD_TITLE])) {
            $task->title = $data[TaskMapper::FIELD_TITLE];
        }
        if (isset($data[TaskMapper::FIELD_DESCRIPTION])) {
            $task->description = $data[TaskMapper::FIELD_DESCRIPTION];
        }
        if (isset($data[TaskMapper::FIELD_REPORTER_ID])) {
            $task->reporter_id = $data[TaskMapper::FIELD_REPORTER_ID];
        }
        if (isset($data[TaskMapper::FIELD_ASSIGNEE_ID])) {
            $task->assignee_id = $data[TaskMapper::FIELD_ASSIGNEE_ID];
        }

        return $task;
    }

}
