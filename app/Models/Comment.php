<?php

namespace TaskSystem\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Comment
 * @package TaskSystem\Models
 */
class Comment extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'task_id',
    ];

}
