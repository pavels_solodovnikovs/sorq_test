<?php

namespace TaskSystem\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Task
 * @package TaskSystem\Models
 */
class Task extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'reporter_id',
        'assignee_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('TaskSystem\Models\Comment');
    }
}
