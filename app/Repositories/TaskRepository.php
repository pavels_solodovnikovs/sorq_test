<?php

namespace TaskSystem\Repositories;

use TaskSystem\Factories\TaskFactory;
use TaskSystem\Mappers\TaskMapper;
use TaskSystem\Models\Task;

/**
 * Class TaskRepository
 * @package TaskSystem\Repositories
 */
class TaskRepository
{

    /**
     * @var TaskFactory
     */
    private $taskFactory;

    /**
     * TaskRepository constructor.
     * @param TaskFactory $taskFactory
     */
    public function __construct(TaskFactory $taskFactory)
    {
        $this->taskFactory = $taskFactory;
    }

    /**
     * @param array $filters
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function get(array $filters)
    {
        $query = Task::query();

        if (isset($filters['assignee_id'])) {
            $query->where(TaskMapper::FIELD_ASSIGNEE_ID, $filters['assignee_id']);
        }
        if (isset($filters['title'])) {
            $query->where(TaskMapper::FIELD_TITLE, $filters['title']);
        }

        return $query->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return Task::find($id);
    }

    /**
     * @param Task $task
     * @return bool
     */
    public function add(Task $task)
    {
        return $task->save();
    }

}
