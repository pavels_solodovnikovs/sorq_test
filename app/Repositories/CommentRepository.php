<?php

namespace TaskSystem\Repositories;

use TaskSystem\Models\Comment;

/**
 * Class CommentRepository
 * @package TaskSystem\Repositories
 */
class CommentRepository
{

    /**
     * @param Comment $comment
     * @return bool
     */
    public function add(Comment $comment)
    {
        return $comment->save();
    }
}
